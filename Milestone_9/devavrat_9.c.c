#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include "node_structure.h"
#include "Milestone9_functions.c"

	int main(){
		
		struct Node *head = NULL,*head1 = NULL;
		struct Node *temp1,*temp2,*temp3;
		struct Node *b = NULL;
		int result,num_elements,num_elements1,element,i,choice,position,temp_var;

		printf("Enter no.of elements :: ");
		if(scanf("%d",&num_elements) == 0){
			printf("Error :: Not a number \n");
			return 0;
		}else if(num_elements == 0){
			printf("Cannot add zero elements\n");
			return 0;
		}else{
			for (i = 0; i < num_elements; i++)
				{
					printf("Enter element %d :: ",i+1);
					scanf("%d",&element);
					push(&head,element);
				}
		
				printf("Your List is :: \n");
				print(head);

				getch();
				element = 0;

				do{
					system("cls");
					printf("MENU:: \n");
					printf("<1>Count-counts occurence of an element !!\n");
					printf("<2>GetNth-gets the element at the given index\n");
					printf("<3>DeleteList-emptys the list\n");
					printf("<4>Pop-pops and displays the first element in the list\n");
					printf("<5>InsertNth-inserts at a specific position\n");
					printf("<6>SortedInsert-takes a sorted list and isnerts element at its correct position\n");
					printf("<7>InsertSort-rearranges the node in sorted order\n");
					printf("<8>Append-appends second list at the end of the first\n");
					printf("<9>FrontBackSplit-splits a list into 2 sub-lists\n");
					printf("<10>RemoveDuplicates-removes duplicates from a sorted list.\n");
					printf("<11>MoveNode-moves front-node of second list to first node \n");
					printf("<12>AlternatingSplit-splits the list alternatively\n");
					printf("<13>ShuffleMerge-merges 2 list alternatively\n");
					printf("<14>SortedMerge-merges 2 lists in increasing order\n");
					printf("<15>MergeSort-takes the list and sorts in increasing order\n");
					printf("<16>SortedIntersect-returns a new list which is intersection of 2 given list\n");
					printf("<17>Reverse-reverses the list\n");
					if(scanf("%d",&choice) == 0){
						printf("Error :: Not a number \n");
						return 0;
					}

					switch(choice){
						case 1:
								printf("\n++++++Count++++++\n");
								printf("Enter an element :: \n");
								if(scanf("%d",&element) == 0){
								printf("Error :: Not a number \n");
								return 0;
								}
								result=count(head,element);


								if(result == 0)
									printf("\n%d is not present in the list !!\n",element);
								else
								printf("\n%d appears %d time(s) in the list\n",element,result);

								printf("Press any to continue.....\n");
								getch();
								break;
						case 2:
								printf("\n+++++GetNth+++++\n");
								printf("Enter value for index(Zero-based indexing) :: \n");
								if(scanf("%d",&element) == 0){
								printf("Error :: Not a number \n");
								return 0;
								}
								if(element < 0 || element > lengthOfStruct(head)-1){
									printf("Error:: Index out of bounds !!\n");
									printf("Press any to continue.....\n");
									getch();
									break;
								}else{
								result = GetNth(head,element);
								printf("%d present at index %d\n",result,element);
									printf("Press any to continue.....\n");
								getch();
								}
								break;
								
						case 3:
								printf("\n+++++DeleteList++++++\n");
								printf("List before calling DeleteList...\n");
								print(head);
								printf("\nList after calling DeleteList...\n");
								DeleteList(&head);
								print(head);
								printf("\nPress any to continue.....\n");
								getch();
								// exit(1);
								break;
						case 4:
								printf("\n+++++Pop+++++\n");
								if(head == NULL){
									printf("List is empty cannot pop anymore elements\n");
									getch();
									break;
								}

								else{printf("List :: \n");
									print(head);
									printf("\nPress any to continue.....\n");
									getch();
									printf("\nOn Calling Pop ...\n");
									result = pop(&head);
									printf("Popped element is %d\n",result );
									printf("List :: \n");
									print(head);
									printf("\nPress any to continue.....\n");
									getch();
								}
															
								break;

						case 5:
									printf("++++++InsertNth++++++\n");
									printf("List :: \n");
									print(head);
									printf("\nEnter element to be inserted and the position(Zero-based indexing)  :: \n");
									if(scanf("%d%d",&element,&position) < 2){
										printf("Error :: Not a number \n");
										return 0;
								}
									InsertNth(&head,element,position);
									printf("List :: \n");
									print(head);
									printf("\nPress any to continue.....\n");
								getch();
								break;
						case 6:
									printf("+++++SortedInsert++++++\n");
									temp1 = head;
									while(temp1 != NULL){
										temp2 = temp1->next;

										while(temp2 != NULL){

											if(temp1->data > temp2->data){
												
												temp_var = temp2->data;
												temp2->data = temp1->data;
												temp1->data = temp_var;

											}

											temp2 = temp2->next;
										}

										temp1 = temp1->next;
									}

									printf("Sorted list is ::\n");
									print(head);

									printf("\nEnter new element :: \n");
									if(scanf("%d",&element) == 0){
									printf("Error :: Not a number \n");
									return 0;
										}
									temp1 =(struct Node*)malloc(sizeof(struct Node));

									temp1->data = element;
									SortedInsert(&head,temp1);

									printf("\nList after Insertion ::\n");
									print(head);

									printf("\nPress any to continue.....\n");
								getch();

						break;
						case 7:
									printf("+++++InsertSort++++++");
										printf("\nEnter no.of elements :: ");
										if(scanf("%d",&num_elements1) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements1 == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements1; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&head1,element);
												}
											}
		
												printf("Your List is :: \n");
												print(head1);


												InsertSort(&head1);

												printf("\nYour List after Insertion sort is :: \n");
												print(head1);
												
								getch();
						break;

						case 8:
									printf("+++++Append++++++\n");
									temp1 = NULL;temp2 = NULL;
										printf("\nEnter no.of elements in first list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your first list is :: \n");
												print(temp1);

										printf("\nEnter no.of elements in second list :: ");
										if(scanf("%d",&num_elements1) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements1 == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements1; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp2,element);
												}
											}
		
												printf("Your second list is :: \n");
												print(temp2);

												Append(&temp1,&temp2);

												printf("\nAfter Append\n");

												print(temp1);	

												getch();

						break;
						case 9:
										printf("+++++++FrontBackSplit+++++++\n");
										temp1 = NULL;temp2 = NULL;
										printf("\nEnter no.of elements in list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your list is :: \n");
												print(temp1);

												FrontBackSplit(temp1,&temp2,&temp3);

												getch();


						break;
						case 10:
											printf("++++++RemoveDuplicates++++++\n");
											temp1 = NULL;
											printf("\nEnter no.of elements in list :: ");
											if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your list is :: \n");
												print(temp1);

												InsertSort(&temp1);

											printf("\nSorted list is :: \n");

											print(temp1);

											RemoveDuplicates(temp1);

											printf("\nSorted list after removing duplicates:: \n");

											print(temp1);


											getch();



						break;

						case 11:
											printf("++++++MoveNode++++++\n");

											temp1 = NULL;temp2 = NULL;
										printf("\nEnter no.of elements in first list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your first list is :: \n");
												print(temp1);

										printf("\nEnter no.of elements in second list :: ");
										if(scanf("%d",&num_elements1) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements1 == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements1; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp2,element);
												}
											}
		
												printf("Your second list is :: \n");
												print(temp2);

											printf("\nAfter calling MoveNode\n");

											MoveNode(&temp1,&temp2);

											printf("List 1 ::\n");
											print(temp1);
											printf("\nList 2 ::\n");
											print(temp2);


											getch();


						break;
						case 12:
											printf("++++++AlternatingSplit++++++\n");
											temp1 = NULL;temp2 = NULL;temp3 = NULL;

											printf("\nEnter no.of elements in list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your list is :: \n");
							 					print(temp1);


												while(temp1 != NULL){
													MoveNode(&b,&temp1);
												} //reverses the list


												AlternatingSplit(b,&temp2,&temp3);

												printf("\nOn Calling AlternatingSplit...\n");
												printf("First List ::\n");
												print(temp2);
												printf("\nSecond List ::\n");
												print(temp3);

												getch();


						break;
						case 13:
												printf("++++++++ShuffleMerge+++++++\n");
												temp1 = NULL;temp2 = NULL,temp3 = NULL;
										printf("\nEnter no.of elements in first list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your first list is :: \n");
												print(temp1);

										printf("\nEnter no.of elements in second list :: ");
										if(scanf("%d",&num_elements1) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements1 == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements1; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp2,element);
												}
											}
		
												printf("Your second list is :: \n");
												print(temp2);

												temp3 = ShuffleMerge(temp1,temp2);

												printf("\nYour List :: \n");
												while(temp3 != NULL){
													MoveNode(&b,&temp3);
												} 
												print(b);


												temp1 = NULL;temp2 = NULL;
												free(temp3);
												free(b);
												b = NULL;
												temp3 = NULL;

												getch();

												break;
						case 14:
												printf("++++++++SortedMerge+++++++\n");
												temp1 = NULL;temp2 = NULL,temp3 = NULL;
										printf("\nEnter no.of elements in first list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}

											sort(temp1);
									printf("Your first list is (sorted) :: \n");
												print(temp1);

										printf("\nEnter no.of elements in second list :: ");
										if(scanf("%d",&num_elements1) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements1 == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements1; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp2,element);
												}
											}

											sort(temp2);
		
												printf("Your second list is (sorted) :: \n");
												print(temp2);

												temp3 = SortedMerge(temp1,temp2);

												printf("\nYour merged list :: \n");

												while(temp3 != NULL){
													MoveNode(&b,&temp3);
												} //reverses the list

												print(b);
												b = NULL;
												getch();

						break;	

						case 15:
												printf("+++++++MergeSort++++++++\n");

												temp1 = NULL;temp2 = NULL;
										printf("\nEnter no.of elements in list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}
		
												printf("Your list is :: \n");
												print(temp1);

												MergeSort(temp1);
												// printf("\nSorted list :: \n");
												// print(temp1);

												getch();

						break;
						case 16:
											printf("++++++SortedIntersect++++++++\n");
											temp1 = NULL;temp2 = NULL,temp3 = NULL;
										printf("\nEnter no.of elements in first list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}

											sort(temp1);
									printf("Your first list is (sorted) :: \n");
												print(temp1);

										printf("\nEnter no.of elements in second list :: ");
										if(scanf("%d",&num_elements1) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements1 == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements1; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp2,element);
												}
											}

											sort(temp2);
		
												printf("Your second list is (sorted) :: \n");
												print(temp2);

											temp3 = SortedIntersect(temp1,temp2);

											printf("\nIntersection of 2 lists ::\n");
											print(temp3);

											getch();

						break;	
						case 17:
											printf("++++++Reverse+++++++\n");
											temp1 = NULL;
										printf("\nEnter no.of elements in list :: ");
										if(scanf("%d",&num_elements) == 0){
											printf("Error :: Not a number \n");
												return 0;
											}else if(num_elements == 0){
													printf("Cannot add zero elements\n");
														return 0;
														}else{
											for (i = 0; i < num_elements; i++)
											{
											printf("Enter element %d :: ",i+1);
											scanf("%d",&element);
											push(&temp1,element);
												}
											}

											printf("Your list is :: \n");
													print(temp1);

											Reverse(&temp1);

											printf("\nOn Reversing ....\n");

											print(temp1);

											getch();



						break;


						default:
								printf("Invalid choice , please try again !!\n");
								getch();
					}

				}while(1);
		
				return 0;

			}

	}