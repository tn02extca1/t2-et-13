#define MAX 50

char* str_copy(const char* str);

char** str_tokenize(char *str, char c);

void *mem_copy(void *dest, const void *src, unsigned int n);