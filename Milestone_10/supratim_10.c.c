#include<stdio.h>
#include<conio.h>
#include<stdlib.h>

#include "Milestone10_functions.h"


int main(){

	struct Entry *head = NULL;
	int choice = 0;
	char meaning[MAX]="", word[MAX]="";
	do{
			system("cls");
			printf("++++++++DICTIONARY++++++++\n");
			printf("MENU ::\n<1>Add a word & its meaning\n<2>Search for a word\n<3>Remove a word\n<4>Print all the words\n");
			if(scanf("%d",&choice) == 0){
			printf("Error :: Not a number \n");
			return 0;
		}


		switch(choice){

			case 1:
					printf("++++ADD A WORD & ITS MEANING++++\n");
					printf("Enter a word :: ");

						str_copy(word,scan_string());
						if(str_length(word) == -1){
					 	break;
						}
										
					printf("\nEnter its meaning :: ");

					str_copy(meaning,scan_string());
						if(str_length(meaning) == -1){
					 	break;
						}

					push(&head,word,meaning);

					printf("\nDictionary contains :: \n");
					print(head);
					word[0] = '\0';
					meaning[0] = '\0';
					printf("Press any key to continue....\n");
					getch();
					break;
			case 2:
					printf("++++SEARCH A WORD++++\n");
					printf("Enter the word to be searched for :: ");
					
					str_copy(word,scan_string());
						if(str_length(word) == -1){
					 	break;
						}

					search(head,word);

					word[0] = '\0';

			break;
			case 3:	printf("++++REMOVE A WORD++++\n");
					printf("Enter a word :: \n");
					
					str_copy(word,scan_string());
						if(str_length(word) == -1){
					 	break;
						}

					Remove(&head,word);

					word[0] = '\0';
			break;
			case 4:
					printf("\nDictionary contains :: \n");
					print(head);

					printf("Press any key to continue....\n");
					getch();
			break;
			default :
			printf("Invalid choice,please try again !!\n");
			getch();
		}

	}while(1);



	return 0;
}