#include<stdio.h>
#include<conio.h>
#include<stdlib.h>

#define MAX 150


struct Entry {
	char word[MAX];
	char meaning[MAX];
	struct Entry *next;
};

int str_copy(char *str1, char *str2) {
	int i = 0;

			while(str2[i] != '\0'){
     	str1[i] = str2[i];
     	i++;
	
} 
str1[i] = '\0';
return 0;
}

int str_length(char *str){
	int i = 0;
	
	if(str){
	while(*(str+i) != '\0'){
		i++;
	}
	    
} 
if(i == 0){
	return -1;
}else{
	return i;
}
}

int str_compare(char *str1, char *str2){

	while(*str1 == *str2 && *str1 != '\0' && *str2 != '\0'){
		str1++;
		str2++;
}
	
	if(*(str1)-*(str2) > 0)
		return 1;
	else if(*(str1)-*(str2) < 0)
		return -1;
	else
		return 0;

}


struct Entry*  push(struct Entry **pointerToHead,char *word,char *meaning){
	struct Entry *temp1;
	struct Entry *new_node = (struct Entry*)malloc(sizeof(struct Entry));


	//Empty List check and addition of first node;
	if(*pointerToHead == NULL){
		str_copy(new_node->word,word);
		str_copy(new_node->meaning,meaning);
		new_node->next = *pointerToHead;
		*pointerToHead = new_node;
	return *pointerToHead ;
	}
	else{

		temp1 = *pointerToHead;
		
		while(temp1->next != NULL) //temp->next is used since we want to stay at the last node !!
			temp1 = temp1->next;
	
		str_copy(new_node->word,word);
		str_copy(new_node->meaning,meaning);
		new_node->next = temp1->next;
		temp1->next = new_node;
	}
	

		return *pointerToHead;
}

void print(struct Entry *head){
	struct Entry *temp = head;
	int i = 1;
	if(temp == NULL){
		printf("\nDictionary is empty !!\n");
		return;
	}else{

	while(temp != NULL){
		printf("\nWord %d = %s\n",i, temp->word);
		printf("It's meaning = %s\n\n", temp->meaning );
		temp = temp->next;
		i++;
	}

}
}

int search(struct Entry *headRef,char *word){
	struct Entry *temp = headRef;
	int i = 1;

	if(temp == NULL){
		printf("\nDictionary is empty !!\n");
		getch();
		return 0;
	}

	while(temp != NULL){

		if(str_compare(temp->word,word) == 0){
			printf("\n%s found at position %d\n",word,i);
			printf("It's meaning is - %s\n",temp->meaning );
			getch();
			return i;
		}
		i++;
		temp = temp->next;
	}

	if(temp == NULL){
		printf("\nSorry, %s not present in the Dictionary !!\n",word);
		getch();
		return 0;
	}

	return 0;
}

void Remove(struct Entry **pointerToHead,char *word){

	struct Entry *temp = *pointerToHead,*prev = NULL;
	int result = search(*pointerToHead,word);

	if(result){
	while(temp != NULL){

		if(str_compare(temp->word,word) == 0){

			if(prev == NULL){
				*pointerToHead = temp->next;
				temp->next = NULL;
				free(temp);

				printf("%s removed successfully !!\n",word );
			getch();
				break;
			}

			prev->next = temp->next;
			temp->next = NULL;
			free(temp);
			
			printf("%s removed successfully !!\n",word );
			getch();
		}
		prev = temp;
		temp = temp->next;
	}


	
	}

}
//+++++++++++++++++++++++++++++++++++++
char* scan_string(){
		char word_char;
		char *word = (char*)calloc(MAX,sizeof(char));
		int i = -1;

	while((word_char=getch()) != '\r'){
						putchar(word_char);
						word[++i] = word_char;
					}
					if(i == -1){
						printf("Invalid input !!\n");
						getch();
						return word;
					}else{
						word[++i] = '\0';
						i = -1;
					}
					 
				return word;
}