#include<stdio.h>
#include<conio.h>
#include "fun.h"
#include<stdlib.h>

	int main(){
	
		char input[MAX] = "",second_input[MAX]="";
		char ch;
		int return_value,choice;


		printf("\nEnter a string :: ");
		scanf("%[^\n]s", input);

		return_value = str_length(input);
		if(return_value == -1){
		printf("\nError : No input provided !");
		getch();
		return 0;
	}

		do{
			printf("\nMENU :: \n");
			printf("1=>To find length of the string.\n");
			printf("2=>To copy the string.\n");
			printf("3=>To compare two strings.\n");
			printf("4=>To find a character in a string.\n");
			printf("5=>To find a sub-string from the given string.\n");
			printf("6=>Quit\n");
			printf("Enter a choice\n");
			scanf("%d",&choice);

				
					switch(choice){

					case 1:
							return_value = str_length(input);
							if(return_value > 0){
								printf("\nLength of %s is %d",input,return_value);
							}
							getch();
							break;
					case 2:
							printf("\nDestination string before copying :: %s",second_input);
							return_value = str_copy(second_input,input);
							if(return_value == 0){
								printf("\nStrings Copied Successfully !!");
								printf("\nDestination string after copying :: %s",second_input);
							}
							else if(return_value == -1)
								printf("\nError in copying strings!!");
		
	
							getch();
							break;
					case 3:
							printf("\nEnter second string to compare :: ");
							scanf(" %[^\n]s",second_input);
							
							
							return_value = str_compare(input,second_input);
							if(return_value == 1){
							printf("%s > %s",input,second_input);
							}else if(return_value == -1){
							printf("%s < %s",input,second_input);
							}else if(return_value == 0){
							printf("%s = %s",input,second_input);
							}
							getch();
							break;
					case 4:
					printf("Enter a character to be searched for :: ");
					scanf(" %c",&ch);
					return_value = str_find_char(input,&ch);

					if(return_value == -1)
						printf("\n%c not present in %s\n",ch,input);
					else 	
						printf("\n%c present at position %d in %s\n",ch,++return_value,input);
							getch();
							break;
					case 5:
							printf("\nEnter a substring to be searched for :: ");
							scanf("%s",second_input);
							return_value = str_find_substring(input,second_input);

							if(return_value == -1)
							printf("\n%s not present in %s",second_input,input);
							else
							printf("\n%s present at position %d in %s",second_input,return_value,input);

							getch();
							break;
					case 6:
                            goto exit_loop;
					default:
							printf("Invalid value entered !!\n");
							getch();

				}

				
				
		}while(1);
		exit_loop:printf("\nPress any key to continue....");
		getch();
		
		return 0;
	}
